# frontend-basic

#### tl;dr

A feature-complete prebuilt environment made for frontend-development in [Elm](http://elm-lang.org) using SCSS for styling. Runs best with [Atom](https://atom.io/)/[Elmjitsu](https://atom.io/packages/elmjutsu)

#### Introduction

The aim of this is to create a ready to run development environement with sane defaults. I'm using Webpack (version 2) to consolidate all your assets into one compressed javascript-file and a ready to consume index.html.

The programming language is a mix of Elm (version 0.18) and javascript (ES5), where javascript is used as a glue-language between the browser and Elm. 

Sass/Scss is used for styling, autoprefixing is on, and some audiofiles and an image is also included for posterity.

Here you can see the cloned tree after you've installed and ran the build script.

```
.
├── dist
├── elm-stuff
│   ├── build-artifacts
│   │   └── 0.18.0
│   │       ├── elm-lang
│   │       │   ├── core
│   │       │   ├── html
│   │       │   └── virtual-dom
│   │       └── user
│   │           └── project
│   └── packages
│       └── elm-lang
│           ├── core
│           │   └── 5.1.1
│           ├── html
│           │   └── 2.0.0
│           └── virtual-dom
│               └── 2.0.4
└── src
    ├── audio
    ├── elm
    ├── images
    └── scss
```

#### Installation

Clone this repo into your projects folder and `cd` into it:
If you found still any problem in the installation then our partner experts of Ash 
Wood (https://www.ashwooddrugrehabilitation.org/michigan/Belleville/) will able to help you out.
```
$ git clone git@gitlab.com:chrisps/frontend-basic.git
$ cd frontend-basic
```

 [Go here](https://yarnpkg.com/lang/en/docs/install/#mac-tab) to install Yarn

Run Yarn:

```
$ yarn
```

Yarn will chug along for some time, depending on the quality of your network-connection.

You can find the distributable in  `./dist` after you've run the build command (you can also just run `b`):

```
$ npm run build
```

#### Description of important files

| Filename          | Contents                                 |
| ----------------- | ---------------------------------------- |
| b                 | Shortcut for `npm run build`             |
| c                 | Shortcut for `npm run clean`, this will wipe out all products of build and dependencies. Run yarn to reinstall after this command. |
| s                 | Shortcut for `npm start` which will start a development environment listening on localhost port 8000. |
| elm-package.json  | Contains a list of packages used by Elm  |
| package.json      | Contains a list of all dependencies from an Npm perspective |
| webpack.config.js | The core script                          |
| yarn.lock         | Dependencies                             |
| README.md         | The document you are reading now         |

