require("./scss/styles.scss")
require('./audio/event1.mp3');
require('./audio/sample.mp3');
require('./images/bg.jpg');

var Elm = require('./elm/Main.elm');
var mountNode = document.getElementById('main');
var app = Elm.Main.embed(mountNode);

app.ports.audioSignal.subscribe(function(t) {
    audioSignal(t)
});

function audioSignal(t) {
    switch (t) {
        case 1:
            mysample.play()
            break;
        case 2:
            mysample2.play()
            break;
        default:
            console.log("do nothing")
    }
}

var audioContext = new (window.AudioContext || window.webkitAudioContext)();
var mysample = document.querySelector(".soundfile1");
var mysample2 = document.querySelector(".soundfile2");
var source = audioContext.createMediaElementSource(mysample);
var source2 = audioContext.createMediaElementSource(mysample2);
source.connect(audioContext.destination);
source2.connect(audioContext.destination);
